<?php

namespace Health\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class HealthServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes.php');
        $this->commands([
            \Health\Commands\QueueHealth::class
        ]);
        // $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configure();
        $schedule = app(Schedule::class);

        $this->app->make('Health\Controllers\HealthController');
        $this->app->booted(function () {
            $schedule = app(Schedule::class);
            $schedule->command('health:queue')->everyMinute();
        });
    }

    protected function configure()
    {
        $this->mergeConfigFrom(__DIR__.'/../Config/healthcheck.php', 'healthcheck');
        $configPath =  $this->app->basePath() . '/config/healthcheck.php';
        $this->publishes([
            __DIR__.'/../Config/healthcheck.php' => $configPath,
        ], 'config');

        if ($this->app instanceof \Laravel\Lumen\Application) {
            $this->app->configure('healthcheck');
        }
    }
}
