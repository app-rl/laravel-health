<?php

namespace Health\Commands;

use queue;
use Illuminate\Console\Command;
use Health\Jobs\QueueHealthJob;
use Illuminate\Support\Facades\Cache;

class QueueHealth extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'health:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        QueueHealthJob::dispatch();
    }
}
