<?php

return [
    /**
     * List of health checks to run when determining the health
     * of the service
     */
    'checks' => [
        'redis' => [
            'class' => 'Health\Checkers\RedisHealth'
         ],
        'database' => [
           'class' => 'Health\Checkers\DatabaseHealth'
        ],
       'chache' => [
           'class' => 'Health\Checkers\CacheHealth'
        ],
       'queue' => [
           'class' => 'Health\Checkers\QueueHealth'
        ],
       'elasticSearch' => [
           'class' => 'Health\Checkers\ElasticSearchHealth',
           'url' => env('SCOUT_ELASTIC_HOST')
        ],
    ],
];
