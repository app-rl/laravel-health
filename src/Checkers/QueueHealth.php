<?php

namespace Health\Checkers;

use Faker\Provider\Base;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class QueueHealth extends Base
{
    /**
     * @return Result
     */
    public static function check()
    {
        $status = 200;

        try {
            $value = Cache::get('healthcheck-queue');

            if(!$value) {
                $status = -1;
            }

        } catch (\Exception $e) {
            $status = -1 . '  ---ERROR---  '. $e->getMessage();
        }

        return $status;
    }

}
