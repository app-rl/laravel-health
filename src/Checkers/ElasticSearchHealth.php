<?php

namespace Health\Checkers;

use Faker\Provider\Base;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class ElasticSearchHealth extends Base
{
    /**
     * @return Result
     */
    public static function check()
    {
        $status = 200;

        try {
            $client = ClientBuilder::create()
                ->setHosts([config('healthcheck.checks.elasticSearch.url')])
                ->build();

            $health = $client->cluster()->health() ?? null;
            if($health && $health['status'] == 'red') {
                $status = -1;
            }

        } catch (\Exception $e) {
            $status = -1 . '  ---ERROR---  '. $e->getMessage();
        }

        return $status;
    }

}
