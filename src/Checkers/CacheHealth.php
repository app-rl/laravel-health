<?php

namespace Health\Checkers;

use Faker\Provider\Base;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;


class CacheHealth extends Base
{
   public static function check()
    {
        $status = 200;
        try {
            $checker = 'DUMMY DATA ';

            Cache::forget('healthcheck');

            $value1 = Cache::remember('healthcheck', 10,function () use($checker){
                return $checker ;
            });

            $value2 = Cache::remember('healthcheck', 10,function () use($checker){
                return $checker .'CPY' ;
            });
            // dd('sss',$value2, $value1, $checker);

            if ($value1 !== $value2 || $value2 !== $checker) {
                $status = -1;
            }

        } catch (\Exception $e) {
            $status = -1 .  '  ---ERROR---  '. $e->getMessage();;
        }
        return $status;
    }

    private function getCached()
    {
        $checker = $this->getChecker();
        $value = Cache::rememberForever('healthcheck', $seconds, $checker);
        return $value;

    }

    private function getChecker()
    {
        return 'DUMMY DATA'. date('Y-m-d H:i');
    }
}
