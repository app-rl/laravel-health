<?php

namespace Health\Checkers;

use Faker\Provider\Base;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class RedisHealth extends Base
{
    /**
     * @return Result
     */
    public static function check()
    {
        $status = 200;

        try {
            Redis::ping();
        } catch (\Exception $e) {
            $status = -1 . '  ---ERROR---  '. $e->getMessage();
        }

        return $status;
    }

}
