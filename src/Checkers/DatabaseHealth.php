<?php

namespace Health\Checkers;

use Faker\Provider\Base;
use Illuminate\Support\Facades\DB;


class DatabaseHealth extends Base
{
    /**
     * @return Result
     */
    public static function check()
    {
        $status = 200;

        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            $status = -1 .'  ---ERROR---  '. $e->getMessage();;
        }
        return $status;
    }

}
