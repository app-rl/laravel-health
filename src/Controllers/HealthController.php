<?php

namespace Health\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;

class HealthController extends Controller
{

    public function checkAllService()
    {
        $statusArray = [];
        $checkeds = config('healthcheck.checks');
        foreach ($checkeds as $key => $service) {
            try {
                $statusService = $service['class']::check();
                $statusArray[$key] = $statusService;

            } catch (\Throwable $th) {
                $statusArray[$key] = $th;
            }
        }

        $statusResponse = collect($statusArray)->filter(function ($v) {
            return $v != 200;
        })->first() ? 500 : 200;

        return response()->json($statusArray, $statusResponse);
    }
}
